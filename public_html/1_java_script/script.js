function postData() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_asistencias = document.getElementById('resultados_asistencias');
      resultados_asistencias.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Fecha', 'Hora', 'Nombre', 'Apellido', 'DNI', 'Asistencia'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });
    
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_asistencias.appendChild(table);
      } else {
        resultados_asistencias.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

function postData1() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_asistencias = document.getElementById('resultados_asistencias');
      resultados_asistencias.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Hora', 'Nombre', 'Apellido', 'DNI', 'Asistencia'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });
    
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_asistencias.appendChild(table);
      } else {
        resultados_asistencias.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

function postData2() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_asistencias_fechas = document.getElementById('resultados_asistencias_fechas');
      resultados_asistencias_fechas.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Fecha', 'Hora', 'Nombre', 'Apellido', 'DNI', 'Asistencia'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });
    
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_asistencias_fechas.appendChild(table);
      } else {
        resultados_asistencias_fechas.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}
function postData3() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_materias = document.getElementById('resultados_materias');
      resultados_materias.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Dia', 'Materia', 'Horario', 'Profesor a cargo'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });
    
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_materias.appendChild(table);
      } else {
        resultados_materias.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

function postData4() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_asistencias_materias = document.getElementById('resultados_asistencias_materias');
      resultados_asistencias_materias.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Dia', 'Materia', 'Horario', 'Profesor a cargo', 'Hora de acceso', 'Asistencia'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });
    
          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_asistencias_materias.appendChild(table);
      } else {
        resultados_asistencias_materias.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}


 // Función para obtener los datos de la base de datos y llenar el desplegable
 function llenarDesplegable() {
  fetch('../1_php/obtener_datos.php')
      .then(response => response.json())
      .then(data => {
          var desplegable = document.getElementById('opciones');
          data.forEach(dato => {
              var option = document.createElement('option');
              option.value = dato.curso;
              option.text = dato.curso;
              desplegable.appendChild(option);
          });
      })
      .catch(error => console.error('Error:', error));
}

window.onload = llenarDesplegable;