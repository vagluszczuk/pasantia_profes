<?php
  // Conexión a la base de datos
  $servername = "localhost";
  $username = "admin";
  $password = "";
  $dbname = "proyecto_asistencias_profes";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['apellido'])){
    $apellido = $_POST['apellido'];
  
    if($apellido=="todos" or $apellido=="all"){
      $sql = "SELECT datos_profesores.codigo, datos_profesores.nombre, datos_profesores.apellido, datos_profesores.CUIL, datos_profesores.lunes, datos_profesores.martes, datos_profesores.miercoles, datos_profesores.jueves, datos_profesores.viernes
              FROM datos_profesores
              ORDER BY datos_profesores.codigo ASC";

    }else{
      $sql = "SELECT datos_profesores.codigo, datos_profesores.nombre, datos_profesores.apellido, datos_profesores.CUIL, datos_profesores.lunes, datos_profesores.martes, datos_profesores.miercoles, datos_profesores.jueves, datos_profesores.viernes
              FROM datos_profesores
              WHERE datos_profesores.apellido LIKE '%$apellido%'
              ORDER BY datos_profesores.codigo ASC";
    }

      $result = $conn->query($sql);
    
     
      if ($result->num_rows > 0) {
        // Almacenar los resultados en un array
        $results_array = array();
        while($row = $result->fetch_assoc()) {
          // Verificar si cada valor es nulo antes de agregarlo al array y reemplazarlo con "-"
          $codigo = ($row["codigo"] !== null) ? $row["codigo"] : "-";
          $nombre = ($row["nombre"] !== null) ? $row["nombre"] : "-";
          $apellido = ($row["apellido"] !== null) ? $row["apellido"] : "-";
          $CUIL = ($row["CUIL"] !== null) ? $row["CUIL"] : "-";
          $lunes = ($row["lunes"] !== null) ? $row["lunes"] : "-";
          $martes = ($row["martes"] !== null) ? $row["martes"] : "-";
          $miercoles = ($row["miercoles"] !== null) ? $row["miercoles"] : "-";
          $jueves = ($row["jueves"] !== null) ? $row["jueves"] : "-";
          $viernes = ($row["viernes"] !== null) ? $row["viernes"] : "-";
          
          $results_array[] = array(
            "codigo" => $codigo,
            "nombre" => $nombre,
            "apellido" => $apellido,
            "CUIL" => $CUIL,
            "lunes" => $lunes,
            "martes" => $martes,
            "miercoles" => $miercoles,
            "jueves" => $jueves,
            "viernes" => $viernes
          );
        }
       
        // Enviar los resultados como respuesta en formato JSON
        header('Content-Type: application/json');
        echo json_encode($results_array);
      } else {
        echo "No se encontraron resultados para la fecha seleccionada.";
      }
      
  }

  // Cerrar la conexión a la base de datos
  $conn->close();
?>