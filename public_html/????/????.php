<?php
  // Conexión a la base de datos
  $servername = "localhost";
  $username = "admin";
  $password = "";
  $dbname = "proyecto_asistencias_profes";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['apellido'])){
    $apellido = $_POST['apellido'];
  
    $sql = "SELECT datos_profesores.codigo, datos_profesores.nombre, datos_profesores.apellido, datos_profesores.dni, datos_profesores.lunes, datos_profesores.martes, datos_profesores.miercoles, datos_profesores.jueves, datos_profesores.viernes
            FROM datos_profesores
          WHERE datos_profesores.apellido = '$apellido'";
  

    $result = $conn->query($sql);
    echo $sql;
    echo $result;
    if ($result->num_rows > 0) {
      // Almacenar los resultados en un array
      $results_array = array();
      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "codigo"=>$row["codigo"],
          "nombre"=>$row["nombre"],
          "apellido"=>$row["apellido"],
          "dni"=>$row["dni"],
          "lunes"=>$row["lunes"],
          "martes"=>$row["martes"],
          "miercoles"=>$row["miercoles"],
          "jueves"=>$row["jueves"],
          "viernes"=>$row["viernes"]
      );
      }
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }

  // Cerrar la conexión a la base de datos
  $conn->close();
?>