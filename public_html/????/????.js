function postData() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      console.log('response:', this.responseText);

      const resultados_datos_profes = document.getElementById('resultados_datos_profes');
      resultados_datos_profes.innerHTML = '';
      const data = JSON.parse(this.responseText);
      console.log('data:', data);

      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Codigo', 'Nombre', 'Apellido', 'DNI', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });

          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        resultados_datos_profes.appendChild(table);
      } else {
        resultados_datos_profes.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };

  console.log('form.action:', form.action);
  xhr.open('POST', form.action, true);
  if (formData.length > 0) {
    console.log('formData:', formData);
    xhr.send(formData);
  }
  resultados_datos_profes.textContent = 'estamos teniendo un problema capo';
}
  
  
  
  