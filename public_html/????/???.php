<?php
  // Conexión a la base de datos
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "proyecto_asistencias_profes";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['apellido_seleccionado'])){
    $apellido_seleccionado = $_POST['apellido_seleccionado'];

   

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {

      // Almacenar los resultados en un array
      $results_array = array();

      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "fecha"=>$row["fecha"],
          "hora"=>$row["hora"],
          "nombre"=>$row["nombre"],
          "apellido"=>$row["apellido"],
          "dni"=>$row["dni"],
          "asistencia"=>$row["asistencia"]
      );
      }

      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }
  // Cerrar la conexión a la base de datos
  $conn->close();
?>